-- 1. check nat num is even
data Nat : Set where
  zero : Nat
  suc  : Nat -> Nat
  
data Bool : Set where
  true  : Bool
  false : Bool

data Valid : Bool → Set where
  check : Valid true

_+_ : Nat -> Nat -> Nat
zero + n = n
(suc m) + n = suc (m + n)

{-# BUILTIN NATURAL Nat #-}

even? : Nat -> Bool
even? zero = true
even? (suc zero) = false
even? (suc (suc n)) = even? n

-- x0 = even? 3

-- 2. valid check

-- _ : Valid true
-- _ = check

not : Bool -> Bool
not true = false
not false = true

odd? : (n : Nat) → Valid (even? n)
                 → Valid (not (even? (suc n)))
odd? zero check = check
odd? (suc zero) ()
odd? (suc (suc n)) p = odd? n p

-- Valid (even? (suc (suc n))) == Valid (even? n)

x1 = odd? 3

{-- 
  3. prove two function f(x) - g(x) = 0, then f(x) = g(x)
--}

data _≡_ {A : Set} (x : A) : A → Set where
  refl : x ≡ x

-- cong== : ∀ {A B : Set} (f : A -> B) {x y : A} -> x ≡ y -> f x ≡ f y
-- cong== f refl = refl

cong : ∀ {A B : Set} (f g : A → B) {x y : A} → f ≡ g → ∀ (x : A) → f x ≡ g x
cong f g refl = λ x → refl
