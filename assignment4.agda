{- 
1. Define the reverse function on lists, both the naive(??) and the tail-recursive version. (2pt)
2. Show that the naive list reverse function is involutive, i.e. reverse (reverse xs) ≡ xs. (2pt)
    Hint: You will need an additional lemma (auxiliary result) showing that reverse (xs ++ ys) ≡ (reverse ys) ++ (reverse xs).
3. Show that the naive list reverse function and the tail-recursive version are equal. (1pt, harder)
-}

data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A

data Nat : Set where
  zero : Nat
  suc  : Nat → Nat
infixr 4 _∷_
infixr 5 _++_
infixr 10 _≡_

_++_ : ∀ {A : Set} → List A → List A →  List A
_++_ [] ys = ys
_++_ (x ∷ xs) ys = x ∷ (xs ++ ys)

data _≡_ {A : Set} : A → A → Set where
  refl : (x : A) → x ≡ x
  
{- reverse : ∀ {A : Set} → List A → List A → List A
reverse [] ys = ys
-- reverse [] (x ∷ xs) = reverse xs xs 
reverse (x ∷ xs) [] = reverse xs xs
reverse (x ∷ xs) (y ∷ ys) = reverse ys xs -}

-- The naive reverse
reverse : {A : Set} -> List A -> List A
reverse [] = []
reverse (x ∷ xs) = reverse xs ++ (x ∷ [])

-- The tail-recursive reverse
tail-reverse : {A : Set} -> List A -> List A
tail-reverse xs = tail-reverse' xs [] where 
  tail-reverse' : {A : Set} → List A → List A → List A
  tail-reverse' [] acc = acc
  tail-reverse' (x ∷ xs) acc = tail-reverse' xs (x ∷ acc) 

cong-≡ : {A B : Set} → (f : A → B) → (xs ys : A) → xs ≡ ys → f xs ≡ f ys
cong-≡ f xs ys (refl .xs) = refl (f xs)


e : {A : Set} → List A
e = []

p0 : ∀ {A : Set} → (x : List A) → (x ++ e) ≡ x
p0 [] = refl []
p0 (x ∷ xs) = cong-≡ (_∷_ x) ((xs ++ e)) xs (p0 xs)

p2 : ∀ {A : Set} → (xs ys zs : List A) → ((xs ++ ys) ++ zs) ≡ (xs ++ (ys ++ zs))
p2 [] ys zs = refl (ys ++ zs)
p2 (x ∷ xs) ys zs = cong-≡ (_∷_ x) ((xs ++ ys) ++ zs) (xs ++ ys ++ zs) (p2 xs ys zs)

sym-≡ : {A : Set} → ∀ (x y : A) → x ≡ y → y ≡ x
sym-≡ x .x (refl .x) = refl x 

auxi- : {A : Set} (xs ys : List A) → reverse (xs ++ ys) ≡ (reverse ys ++ reverse xs)
auxi- [] ys = sym-≡ (reverse ys ++ []) (reverse ys) (p0 (reverse ys))  --symmi
-- auxi- (x ∷ xs) ys = sym-≡ (reverse ys ++ reverse xs ++ (x ∷ [])) (reverse (xs ++ ys) ++ (x ∷ [])) (auxi- (reverse {!!}) {!!})
-- fin auxi- (x ∷ xs) ys = sym-≡ (reverse ys ++ reverse xs ++ (x ∷ [])) (reverse (xs ++ ys) ++ (x ∷ [])) (p2 (reverse [] ) (reverse (xs ∷ [])) reverse ys)
auxi- (x ∷ xs) ys = sym-≡ (reverse ys ++ reverse xs ++ (x ∷ [])) (reverse (xs ++ ys) ++ (x ∷ [])) (p2 (reverse []) (reverse {!(x ∷ xs)!}) (reverse ys))
-- fin 2 auxi- (x ∷ xs) ys = sym-≡ (reverse ys ++ reverse xs ++ (x ∷ [])) (reverse (xs ++ ys) ++ (x ∷ [])) (p2 (reverse ys) (reverse xs) (p0 (reverse x)))
-- auxi- (x ∷ xs) ys = sym-≡ (reverse ys ++ reverse xs ++ (x ∷ [])) (reverse (xs ++ ys) ++ (x ∷ [])) (auxi- (tail-reverse ((x ∷ []))) (tail-reverse {!!}))
-- auxi- (x ∷ xs) [] = sym-≡ (reverse ys ++ reverse xs ++ (x ∷ [])) (reverse (xs ++ ys) ++ (x ∷ [])) (p2 (reverse []) (reverse ?) (reverse ?))
-- auxi- (x ∷ xs) (x₁ ∷ ys) = sym-≡ (reverse ys ++ reverse xs ++ (x ∷ [])) (reverse (xs ++ ys) ++ (x ∷ [])) (p2 (reverse []) (reverse ?) (reverse ?))))
--  (reverse ys)
{-
reverse₁ : ∀ {A : Set} → List A → List A
reverse₁ = λ z → z
auxi-naive : {A : Set} (xs ys : List A) → reverse₁ (reverse₁ xs) ≡ xs
auxi-naive [] = λ ys → refl []
auxi-naive = λ xs ys → refl xs
-}

{- tail-rec : {A : Set} (xs ys : List A) → reverse (xs ++ ys) ≡ reverse xs
tail-rec xs [] = tail-rec' where
  tail-rec' : {A : Set} → List A → List A → List A
  tail-rec' xs ys = ? -}


-- ----------------------------------------
naive-count : {A : Set} → List A → Nat
naive-count [] = zero
naive-count (x ∷ xs) = suc (naive-count xs)

{-# BUILTIN NATURAL Nat #-}

tail-rec-count : {A : Set} → List A → Nat
tail-rec-count xs = tail-rec-count' xs zero where
  tail-rec-count' : {A : Set} → List A → Nat → Nat
  tail-rec-count' [] total  = total
  tail-rec-count' (x ∷ xs) total = tail-rec-count' xs (suc total)

