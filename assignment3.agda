-- 1. Find the identity element e of the Monoid and prove that it is so, on the left and on the right

data List (A : Set) : Set where
  []  : List A
  _∷_ : A → List A → List A

_++_ : ∀ {A : Set} → List A → List A → List A
_++_ [] ys  = ys --Base Case
_++_ (x ∷ xs) ys = x ∷ (xs ++ ys) -- Step Case

data _≡_ {A : Set} : A → A → Set where
  refl : (x : A) → x ≡ x
 
cong-≡ : {A B : Set} → (f : A → B) → (xs ys : A) → xs ≡ ys → f xs ≡ f ys
cong-≡ f xs ys (refl .xs) = refl (f xs)

e : {A : Set} → List A
e = []
-- {- Error
p0 : ∀ {A : Set} → (x : List A) → (x ++ e) ≡ x
p0 [] = refl []
p0 (x ∷ xs) = cong-≡ (_∷_ x) ((xs ++ e)) xs (p0 xs)
-- p0 (x ∷ xs) = cong-≡ {!!} {!!} {!!} {!!}  -- 1. input "cong-≡" 2. ctrl-c, ctrl-c 3. ctrl-c ctrl-r
-- --p0 (x ∷ xs) = cong-≡ (_∷_ x) (xs ++ []) xs (p0 xs) -- 1. input "cong-≡" 2. ctrl-c, ctrl-a
-- p0 (x ∷ xs) = cong-≡ {!!} x x (refl x)  -- succ n m z 
--  (x ∷ (xs ++ e)) ≡ (x ∷ xs)


p1 : ∀ {A : Set} → (x : List A) → (e ++ x) ≡ x
p1 [] = refl []
p1 (x ∷ xs) = refl (x ∷ xs)


p2 : ∀ {A : Set} → (xs ys zs : List A) → ((xs ++ ys) ++ zs) ≡ (xs ++ (ys ++ zs))
p2 [] ys zs = refl (ys ++ zs)
p2 (x ∷ xs) ys zs = cong-≡ (_∷_ x) ((xs ++ ys) ++ zs) (xs ++ ys ++ zs) (p2 xs ys zs)
{-
p2 : ∀ {A : Set} → (xs ys zs : List A) → ((xs ++ ys) ++ zs) ≡ (xs ++ (ys ++ zs))
-- p2 [] = λ ys zs → refl (ys ++ zs)
p2 [] ys zs = refl (ys ++ zs)
p2 (x ∷ xs) _ _ = cong-≡ (_∷_ x) ((xs ++ ys) ++ zs) (xs ++ .ys ++ .zs)
                    (p2 xs .ys .zs)
-}

infixr 10 _≡_

{-
record Monoid (M : Set) : Set where
  infixl 25 _∙_
  field
    _∙_ : M → M → M
    law-- : (x y z : M) → (x ∙ y) ∙ z ≡ x ∙ (y ∙ z)
    e : M
-}

-- 2. Formulate and prove the property of accociativity append.

infixr 5 _++_

{-
asso : ∀ {A : Set} (xs ys zs : List A) → (xs ++ ys) ++ zs ≡ xs ++ (ys ++ zs)
asso [] ys zs = [] ++ (ys ++ zs)
asso (x ∷ xs) ys zs = x ∷ xs ++ (ys ++ zs)
-}

{- class practice
data Nat : Set where
  zero : Nat
  suc : Nat → Nat

{-# BUILTIN NATURAL Nat #-}

_+_ : Nat → Nat → Nat
_+_ zero n = n
_+_ (suc m) n = suc (m + n)

idl-+ : ∀ n → 0 + n ≡ n
idl-+ n = refl n

idlr-+ : ∀ n → n + 0 ≡ n
idlr-+ = ?

-}
